package Escapeguard.Game;

import Escapeguard.Game.Jugador.Jugador;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.scene.image.Image;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MenuController implements Initializable {
    private Scene scene;
    static public Stage stage;
    static public final Image imageSoundOff = new Image(MenuController.class.getResourceAsStream("Images/sound_off.png"));
    static public final Image imageSoundOn = new Image(MenuController.class.getResourceAsStream("Images/sound_on.png"));
    public static boolean sound = true;

    @FXML
    private TextField textFieldNombre;

    @FXML
    private Button buttonSound;


    public void clickJugar(ActionEvent event) throws IOException {
        if (!textFieldNombre.getText().isBlank()) {
            actualizarJugadorActual();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("sample.fxml"));
            Parent root = loader.load();
            scene = new Scene(root);
            GameController gameController = loader.getController();
            stage.setScene(scene);
            stage.setTitle("JUGAR");
            stage.setResizable(false);
            gameController.setScene(scene);
        }
        else {
            textFieldNombre.setText("");
        }
    }

    // Click en el boton de quitar / poner sonido
    public void clickSound() {
        if (sound) {
            ImageView imageViewSoundOff = new ImageView(imageSoundOff);
            imageViewSoundOff.setFitHeight(51.0);
            imageViewSoundOff.setFitWidth(60.0);
            buttonSound.setGraphic(imageViewSoundOff);
            buttonSound.prefHeight(63.0);
            buttonSound.prefWidth(80.0);
            sound = false;
        } else {
            ImageView imageViewSoundOn = new ImageView(imageSoundOn);
            imageViewSoundOn.setFitHeight(51.0);
            imageViewSoundOn.setFitWidth(60.0);
            buttonSound.setGraphic(imageViewSoundOn);
            buttonSound.prefHeight(63.0);
            buttonSound.prefWidth(80.0);
            sound = true;
        }
    }

    // Salir
    public void clickSalir(javafx.event.ActionEvent event) {
        Platform.exit();
    }

    // Actualizo el jugador actual de la partida
    private void actualizarJugadorActual() {
        GameController.jugadorActivo = new Jugador(textFieldNombre.getText());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        actualizarBotonSonido();
    }

    // Actualiza el boton del sonido
    private void actualizarBotonSonido() {
        if (sound) {
            ImageView imageViewSoundOff = new ImageView(imageSoundOn);
            imageViewSoundOff.setFitHeight(51.0);
            imageViewSoundOff.setFitWidth(60.0);
            buttonSound.setGraphic(imageViewSoundOff);
            buttonSound.prefHeight(63.0);
            buttonSound.prefWidth(80.0);
        } else {
            ImageView imageViewSoundOn = new ImageView(imageSoundOff);
            imageViewSoundOn.setFitHeight(51.0);
            imageViewSoundOn.setFitWidth(60.0);
            buttonSound.setGraphic(imageViewSoundOn);
            buttonSound.prefHeight(63.0);
            buttonSound.prefWidth(80.0);
        }
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

}
