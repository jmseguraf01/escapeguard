package Escapeguard.Game.Sprites;

import Escapeguard.Game.Componentes.Pasillo;
import Escapeguard.Game.GameController;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Sprites {
    protected Image image;
    protected int width, height;
    protected int x;
    protected int y;
    protected GraphicsContext graphicsContext;
    protected Pasillo pasillo;

    public Sprites(Image image, int x, int y) {
        this.image = image;
        this.x = x;
        this.y = y;
        this.width = (int) image.getWidth();
        this.height = (int) image.getHeight();
    }

    public Sprites(Image image, int width, int height, int x, int y) {
        this.image = image;
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
    }

    public void move(GraphicsContext graphicsContext) {
        return;
    }

    public void render() {
        graphicsContext.drawImage(image, x, y, width, height);
    }

    public void clear() {
        graphicsContext.clearRect(x, y, width, height);
    }

    public void setGraphicsContext(GraphicsContext graphicsContext) {
        this.graphicsContext = graphicsContext;
    }

    public Pasillo getPasillo() {
        return pasillo;
    }

    public void setPasillo(Pasillo pasillo) {
        this.pasillo = pasillo;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
