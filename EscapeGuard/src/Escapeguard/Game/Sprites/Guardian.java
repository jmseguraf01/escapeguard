package Escapeguard.Game.Sprites;

import Escapeguard.Game.GameController;
import javafx.scene.image.Image;

import static Escapeguard.Game.Sprites.Guardian.Direccion.*;

public class Guardian extends Sprites {
    public static final String primeraImagen = "Escapeguard/Game/Images/Guardian/Derecha/Walk_1.png";
    private final String ruta = "Escapeguard/Game/Images/Guardian/";
    private int numeroImagen = 1;
    private final int totalImagenes = 10;
    private final float velocidadInicial = 2f;
    private float velocidad = velocidadInicial;
    private Direccion direccion = IZQUIERDA;

    enum Direccion {
        DERECHA, IZQUIERDA
    }

    public Guardian(Image image, int x, int y) {
        super(image, x, y);
    }

    public Guardian(Image image, int width, int height, int x, int y) {
        super(image, width, height, x, y);
    }


    @Override
    public void render() {
        definirImagen();
        graphicsContext.drawImage(image, x, y, width, height);
        aumentarMovimiento();
        aumentarImagen();
    }

    // Define la imagen que hay que poner
    private void definirImagen() {
        if (direccion.equals(IZQUIERDA)) {
            image = new Image(ruta + "Derecha/Walk_" + numeroImagen + ".png");
        } else if (direccion.equals(DERECHA)) {
            image = new Image(ruta + "Izquierda/Walk_" + numeroImagen + ".png");
        }
    }

    // Aumenta el movimiento
    private void aumentarMovimiento() {
        // Izquierda
        if (direccion.equals(IZQUIERDA)) {
            x += velocidad;
        }
        // Derecha
        else if (direccion.equals(DERECHA)) {
            x -= velocidad;
        }

        // Llega al inicio
        if (x <= 0) {
            direccion = IZQUIERDA;
        }
        // Llega al final
        else if (x + width >= GameController.widthScreen) {
            direccion = DERECHA;
        }

    }

    // Aumenta la imagen
    private void aumentarImagen() {
        if (numeroImagen + 1 > totalImagenes) {
            numeroImagen = 1;
        } else {
            numeroImagen++;
        }
    }

    // Aumenta la velocidad
    public void aumentarVelocidad() {
        velocidad += 0.5f;
    }

    // Disminuye la velocidad
    public void disminuirVelocidad() {
        velocidad -= 0.5f;
    }

    // Restaura la velocidad
    public void restaurarVelocidad() {
        velocidad = velocidadInicial;
    }
}
