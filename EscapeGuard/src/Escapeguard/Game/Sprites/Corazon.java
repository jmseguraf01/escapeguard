package Escapeguard.Game.Sprites;

import javafx.scene.image.Image;

public class Corazon extends Sprites {
    public static final String ruta = "Escapeguard/Game/Images/corazon.png";
    private int index_X = 0;
    public static final int NUM_SPRITES_WIDTH = 29;

    public Corazon(Image image, int x, int y) {
        super(image, x, y);
        width = (int) (image.getWidth() / NUM_SPRITES_WIDTH);
    }

    public Corazon(Image image, int width, int height, int x, int y) {
        super(image, width, height, x, y);
    }

    @Override
    public void render() {
        graphicsContext.drawImage(image, (index_X % NUM_SPRITES_WIDTH) * width, 0, width, height, x,y, width,height);
        siguienteImagen();
    }

    @Override
    public void clear() {
        graphicsContext.clearRect(x, y, width, height);
    }

    // Pasa a la siguiente imagen
    private void siguienteImagen() {
        // Si llega al final
        if (index_X == NUM_SPRITES_WIDTH) {
            index_X = 0;
        } else {
            index_X++;
        }
    }
}
