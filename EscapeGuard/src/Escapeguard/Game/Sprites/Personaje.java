package Escapeguard.Game.Sprites;

import Escapeguard.Game.Componentes.Habitacion;
import Escapeguard.Game.GameController;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;

import java.util.ArrayList;
import java.util.List;

public class Personaje extends Sprites {
    public static final String ruta = "Escapeguard/Game/Images/personaje.png";
    private int index_X = 0;
    private int index_Y = 3;
    public final int NUM_SPRITES_WIDTH = 9;
    public final int NUM_SPRITES_HEIGHT = 4;
    private final int movimiento = 10;
    private List<Habitacion> habitacionesArriba = new ArrayList<>();
    private List<Habitacion> habitacionesAbajo = new ArrayList<>();
    // Variable de margen que se da para la posicion Y
    private final int margenY = 15;

    public Personaje(Image image, int x, int y) {
        super(image, x, y);
        this.y = (GameController.heightScreen / 2) - ((height / NUM_SPRITES_HEIGHT) / 2);
    }

    public Personaje(Image image, int width, int height, int x, int y) {
        super(image, width, height, x, y);
        // Pongo al personaje en mitad de la pantalla
       this.y = (GameController.heightScreen / 2) - ((height / NUM_SPRITES_HEIGHT) / 2);
    }

    public void setHabitaciones(List<Habitacion> habitaciones) {
        for (int i = 0; i < habitaciones.size(); i++) {
            // Añado arriba
            if (i < (habitaciones.size() / 2)) {
                habitacionesArriba.add(habitaciones.get(i));
            }
            // Añado abajo
            else {
                habitacionesAbajo.add(habitaciones.get(i));
            }
        }
    }

    private void arriba() {
        Habitacion habitacionActual = estaEnHabitacion(habitacionesArriba);
        // Siempre que no este dentro del rango de una habitacion
        if (habitacionActual == null) {
            // Siempre que no se salga del pasillo
            if (y - movimiento >= (pasillo.getPositionY() - margenY)) {
                y -= movimiento;
            }
        }
        // Si esta en el rango de una habitacion
        else {
            // Siempre que no se salga de la habitacion
            if (y - movimiento >= (habitacionActual.getY() - habitacionActual.getHeight())) {
                y -= movimiento;
            }
        }
        index_Y = 0;
    }


    private void derecha() {
        // Siempre que no se salga de la pantalla
        if (x + movimiento <= (pasillo.getWidth() - (width / NUM_SPRITES_WIDTH)) && y + (height / NUM_SPRITES_HEIGHT) - 40 > pasillo.getPositionY() && y + (height / NUM_SPRITES_HEIGHT) < (pasillo.getPositionY() + pasillo.getHeight())) {
            x += movimiento;
        }
        index_Y = 3;
    }

    private void abajo() {
        // Siempre que no se salga de la pantalla
        Habitacion habitacionActual = estaEnHabitacion(habitacionesAbajo);
        // Si no esta en el rango de la habitacion
        if (habitacionActual == null) {
            if (y + movimiento <= (pasillo.getPositionY() + pasillo.getHeight() - height / NUM_SPRITES_HEIGHT)) {
                y += movimiento;
            }
        }
        // Si esta en el rango de una habitacion
        else {
            // Siempre que no se salga de la habitacion
            if (y + movimiento <= habitacionActual.getY()) {
                y += movimiento;
            }
        }
        index_Y = 2;
    }

    private void izquierda() {
        // Siempre que no se salga de la pantalla
        if (x - movimiento >= 0 && y + (height / NUM_SPRITES_HEIGHT) - 40 > pasillo.getPositionY()  && y + (height / NUM_SPRITES_HEIGHT) < (pasillo.getPositionY() + pasillo.getHeight())) {
            x -= movimiento;
        }
        index_Y = 1;
    }

    // Mueve la posicion de la X
    private void moverPosicionX() {
        index_X++;
        if (index_X > NUM_SPRITES_HEIGHT) {
            index_X = 0;
        }
    }


    @Override
    public void render() {
        graphicsContext.drawImage(image,
                (index_X % NUM_SPRITES_WIDTH) * (width/ NUM_SPRITES_WIDTH), (index_Y % NUM_SPRITES_HEIGHT) * (height / NUM_SPRITES_HEIGHT),
                (width / NUM_SPRITES_WIDTH), (height / NUM_SPRITES_HEIGHT),
                x,y,
                (width/ NUM_SPRITES_WIDTH),(height/NUM_SPRITES_HEIGHT));
    }

    @Override
    public void clear() {
        graphicsContext.clearRect(x, y, width / NUM_SPRITES_WIDTH, height / NUM_SPRITES_HEIGHT + 10);
    }

    // Click en una tecla
    public void clickKey(KeyCode code) {
        boolean movimiento = true;
        if (code == KeyCode.W || code == KeyCode.UP) {
            arriba();
        } else if (code == KeyCode.D || code == KeyCode.RIGHT) {
            derecha();
        } else if (code == KeyCode.S || code == KeyCode.DOWN) {
            abajo();
        } else if (code == KeyCode.A || code == KeyCode.LEFT) {
            izquierda();
        } else {
            movimiento = false;
        }
        // Siempre que se haya movido lo vuelvo a dibujar
        if (movimiento) {
            clear();
            render();
            moverPosicionX();
        }
    }

    // Funcion que devuelve la habitacion en la que esta el jugador
    private Habitacion estaEnHabitacion(List<Habitacion> habitacionesArriba) {
        Habitacion habitacionActual = null;
        for (Habitacion habitacion : habitacionesArriba) {
            if (x >= habitacion.getX() && x + (width / NUM_SPRITES_WIDTH) <= (habitacion.getX() + habitacion.getWidth()) ) {
                habitacionActual = habitacion;
            }
        }
        return habitacionActual;
    }
}
