package Escapeguard.Game.Componentes;

import Escapeguard.Game.Sprites.Sprites;
import javafx.scene.image.Image;

public class Meta extends Sprites {
    public static final String ruta = "Escapeguard/Game/Images/meta.png";

    public Meta(Image image, int x, int y) {
        super(image, x, y);
    }

    public Meta(Image image, int width, int height, int x, int y) {
        super(image, width, height, x, y);
    }

    @Override
    public void render() {
        graphicsContext.drawImage(image, x, y, width, height);
    }
}
