package Escapeguard.Game.Componentes;

import Escapeguard.Game.Sprites.Sprites;
import javafx.scene.image.Image;

public class Ladrillo extends Sprites {
    public static final String ruta = "Escapeguard/Game/Images/ladrillo_texture.png";

    public Ladrillo(Image image, int x, int y) {
        super(image, x, y);
    }

    public Ladrillo(Image image, int width, int height, int x, int y) {
        super(image, width, height, x, y);
        this.width = width;
        this.height = height;
    }
}
