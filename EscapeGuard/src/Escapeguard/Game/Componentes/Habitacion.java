package Escapeguard.Game.Componentes;

import Escapeguard.Game.Sprites.Sprites;
import javafx.scene.image.Image;

public class Habitacion extends Sprites {
    public static final String ruta = "Escapeguard/Game/Images/habitacion.png";
    public static final Image image = new Image(ruta);

    public Habitacion(Image image, int x, int y) {
        super(image, x, y);
    }

    public Habitacion(Image image, int width, int height, int x, int y) {
        super(image, width, height, x, y);
    }

}
