package Escapeguard.Game.Componentes;

import javafx.scene.layout.StackPane;

public class Pasillo {
    private int width, height;
    private int positionX, positionY;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    // Configuramos las medidas del pasillo
    public void establecerTamanos(int widthScreen, int heightScreen) {
        // Tamaños
        height = heightScreen / 3;
        width = widthScreen;
        // Posiciones
        positionY = heightScreen / 2 - height / 2;
        positionX = 0;
    }

    // Le ponemos al pasillo las medidas
    public void configurarPasillo(StackPane panelPasillo) {
        panelPasillo.setPrefHeight(height);
        panelPasillo.setPrefWidth(width);
        panelPasillo.setLayoutX(positionX);
        panelPasillo.setLayoutY(positionY);
    }
}
