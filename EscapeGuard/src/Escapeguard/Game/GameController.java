package Escapeguard.Game;

import Escapeguard.Game.Componentes.Habitacion;
import Escapeguard.Game.Componentes.Ladrillo;
import Escapeguard.Game.Componentes.Meta;
import Escapeguard.Game.Componentes.Pasillo;
import Escapeguard.Game.Jugador.Jugador;
import Escapeguard.Game.Sprites.Corazon;
import Escapeguard.Game.Sprites.Guardian;
import Escapeguard.Game.Sprites.Personaje;
import Escapeguard.Game.Utils.CrearComponentes;
import Escapeguard.Game.Utils.Musica;
import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.List;

public class GameController implements Initializable {
    public static final int widthScreen = 1000;
    public static final int heightScreen = 350;
    public static Jugador jugadorActivo;
    private Scene scene;
    private Personaje personaje;
    private Pasillo pasillo;
    private Guardian guardian;
    private AnimationTimer animationTimer;
    public static final Ladrillo tempLadrillo = new Ladrillo(new Image(Ladrillo.ruta), 20,20,0, 0);
    private Corazon[] corazones;
    private int nivel = 1;
    private GraphicsContext graphicsContext;
    private Meta meta;
    private List<Ladrillo> ladrillos;
    private List<Habitacion> habitaciones;
    private final int numeroLadrillos = widthScreen / tempLadrillo.getWidth() * 2;
    private final int vidasIniciales = 3;
    private int vidas = vidasIniciales;
    private final int numeroHabitacionesInicial = 10;
    private int numeroHabitaciones = numeroHabitacionesInicial;
    private CrearComponentes crearComponentes;
    private Musica musica = new Musica();
    private MediaPlayer musicaJuego = null;

    @FXML
    StackPane panelPasillo;
    @FXML
    Canvas mainCanvas;
    @FXML
    Button botonSalir;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // Play Musica
        playMusicaJuego();
        graphicsContext = mainCanvas.getGraphicsContext2D();
        // Defino el tamaño de la pantalla
        mainCanvas.setWidth(widthScreen);
        mainCanvas.setHeight(heightScreen - 50);
        // Cambio la fuente y dibujo el nivel
        graphicsContext.setFont(Font.font("Consolas", 15));
        dibujarNivel();
        // Boton sonido
        configurarBotonSalir();

        // Configuro el pasillo
        pasillo = new Pasillo();
        pasillo.establecerTamanos(widthScreen, heightScreen);
        pasillo.configurarPasillo(panelPasillo);

        // Instancio crear componenetes
        crearComponentes = new CrearComponentes(pasillo);

        // Creo los ladrillos
        ladrillos = crearComponentes.crearListaLadrillos(numeroLadrillos, graphicsContext);
        // Creo las habitaciones
        habitaciones = crearComponentes.crearListaHabitaciones(numeroHabitaciones, graphicsContext);
        // Creo los corazones
        corazones = crearComponentes.crearCorazones(vidas, graphicsContext);

        // Creo el personaje y lo dibujo
        personaje = new Personaje(new Image(Personaje.ruta), 0,0);
        personaje.setPasillo(pasillo);
        // Le paso las habitaciones al personaje
        personaje.setHabitaciones(habitaciones);
        personaje.setGraphicsContext(graphicsContext);

        // Creo el guardian y lo dibujo
        guardian = new Guardian(new Image(Guardian.primeraImagen), 65,78, widthScreen, heightScreen / 2 - (78 / 2));
        guardian.setGraphicsContext(graphicsContext);

        // Dibujo la meta
        meta = new Meta(new Image(Meta.ruta),  widthScreen - 65, pasillo.getPositionY());
        meta.setGraphicsContext(graphicsContext);


        // Animacion en bucle
        animationTimer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                personaje.clear();
                guardian.clear();
                dibujarMeta();
                dibujarCorazon();
                dibujarLadrillos(ladrillos);
                dibujarHabitaciones(habitaciones);
                personaje.render();
                dibujarGuardia();
                comprobarGanador();
            }
        };
        animationTimer.start();


    }

    private void dibujarGuardia() {
        guardian.render();

        // Si el guardian ha pasado la X del personaje
        // * De derecha a izquierda
        if (guardian.getX() <= personaje.getX() + (personaje.getWidth() / personaje.NUM_SPRITES_WIDTH) && guardian.getX() >= personaje.getX()) {
            if (personaje.getY() + (personaje.getHeight() / personaje.NUM_SPRITES_HEIGHT) >= guardian.getY() && personaje.getY() + (personaje.getHeight() / personaje.NUM_SPRITES_HEIGHT) <= guardian.getY() + guardian.getHeight()) {
                jugadorMuerto();
            } else if (personaje.getY() >= guardian.getY() && personaje.getY() <= guardian.getY() + guardian.getHeight()) {
                jugadorMuerto();
            }
        }
        // * De izquierda a derecha
        else if (personaje.getX() <= guardian.getX() + guardian.getWidth() && personaje.getX() + (personaje.getWidth() / personaje.NUM_SPRITES_WIDTH) >= guardian.getX()) {
            if (personaje.getY() + (personaje.getHeight() / personaje.NUM_SPRITES_HEIGHT) >= guardian.getY() && personaje.getY() + (personaje.getHeight() / personaje.NUM_SPRITES_HEIGHT) <= guardian.getY() + guardian.getHeight()) {
                jugadorMuerto();
            } else if (personaje.getY() >= guardian.getY() && personaje.getY() <= guardian.getY() + guardian.getHeight()) {
                jugadorMuerto();
            }
        }

    }

    // Cuando el guardian toca al jugador
    private void jugadorMuerto() {
        restablecerPosiciones();
        // Quito una vida y vuelvo a dibujar los corazones
        limpiarCorazones();
        vidas--;
        corazones = crearComponentes.crearCorazones(vidas, graphicsContext);
        // HA PERDIDO
        if (vidas == 0) {
            haPerdido();
        }
    }

    // Cuando el jugador ha perdido la partida
    private void haPerdido() {
        numeroHabitaciones = numeroHabitacionesInicial;
        nivel = 1;
        restablecerPosiciones();
        restablecerPantalla();
        vidas = vidasIniciales;
        guardian.restaurarVelocidad();
        corazones = crearComponentes.crearCorazones(vidas, graphicsContext);
        dibujarCorazon();
        playMusicaJuego();
    }

    // Restablece las posiciones del jugador y guardian
    private void restablecerPosiciones() {
        // Borro al jugador y guardian de la posicion actual
        guardian.clear();
        personaje.clear();
        // Reseteo la posicion del jugador y del guardian
        guardian.setX(widthScreen - guardian.getWidth());
        personaje.setX(0);
    }

    // Restablece toda la pantalla
    private void restablecerPantalla() {
        graphicsContext.clearRect(0,0, widthScreen, heightScreen);
        ladrillos = crearComponentes.crearListaLadrillos(numeroLadrillos, graphicsContext);
        // Dependiendod el nivel voy quitando habitaciones
        if (nivel == 5) {
            numeroHabitaciones -= 2;
        } else if (nivel == 8) {
            numeroHabitaciones -= 4;
        }
        habitaciones = crearComponentes.crearListaHabitaciones(numeroHabitaciones, graphicsContext);
        personaje.setHabitaciones(habitaciones);
        dibujarNivel();
    }

    private void dibujarHabitaciones(List<Habitacion> habitaciones) {
        for (Habitacion habitacion : habitaciones) {
            habitacion.render();
        }
    }

    private void dibujarLadrillos(List<Ladrillo> ladrillos) {
        for (Ladrillo ladrillo : ladrillos) {
            ladrillo.render();
        }
    }

    private void dibujarCorazon() {
        for (Corazon corazon : corazones) {
            corazon.clear();
            corazon.render();
        }
    }

    private void limpiarCorazones() {
        for (Corazon corazon : corazones) {
            corazon.clear();
        }
    }

    private void dibujarNivel() {
        graphicsContext.clearRect(0,0, 20,20);
        graphicsContext.fillText("Nivel: " + nivel, 2, 20);
    }

    private void dibujarMeta() {
        meta.clear();
        meta.render();
    }

    // Comprueba si el personaje ha ganado
    private void comprobarGanador() {
        // Hay ganador
        if (personaje.getX() > (meta.getX() - meta.getWidth() + (personaje.getWidth() / personaje.NUM_SPRITES_WIDTH) - 10)) {
            jugadorHaGanado();
        }
    }

    // Cuando gana el jugador
    private void jugadorHaGanado() {
        restablecerPosiciones();
        guardian.aumentarVelocidad();
        nivel++;
        restablecerPantalla();
        playMusicaJuego();
    }

    public void setScene(Scene scene) {
        this.scene = scene;
        // Cuando se da click en una tecla
        scene.setOnKeyPressed(keyEvent -> {
            personaje.clickKey(keyEvent.getCode());
        });
    }

    // Reproduce la musica del juego
    private void playMusicaJuego() {
        // Si es la primera vez, inicializo la variable
        if (musicaJuego == null) {
            musicaJuego = musica.juego();
        }
        // Siempre que este activado el volumen
        if (MenuController.sound) {
            musicaJuego.play();
        }
    }

    // Configura el boton salir
    private void configurarBotonSalir() {
        Image image = new Image(MenuController.class.getResourceAsStream("Images/salir.png"));
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(51.0);
        imageView.setFitWidth(60.0);
        botonSalir.setGraphic(imageView);
        botonSalir.prefHeight(63.0);
        botonSalir.prefWidth(80.0);
        botonSalir.setLayoutX(widthScreen - botonSalir.getPrefWidth() - 3);
        botonSalir.setLayoutY(heightScreen - 30);
        botonSalir.getStylesheets().add("styles.css");
    }

    // Click en el boton salir
    public void clickBotonSalir() throws IOException {
        musicaJuego.stop();
        animationTimer.stop();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("menustart.fxml"));
        Parent root = loader.load();
        scene = new Scene(root);
        scene.getStylesheets().add("styles.css");
        MenuController menuController = loader.getController();
        MenuController.stage.setScene(scene);
        MenuController.stage.setTitle("Escape Guard");
        MenuController.stage.setResizable(false);
    }

}
