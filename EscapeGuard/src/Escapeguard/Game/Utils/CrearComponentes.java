package Escapeguard.Game.Utils;

import Escapeguard.Game.Componentes.Habitacion;
import Escapeguard.Game.Componentes.Ladrillo;
import Escapeguard.Game.Componentes.Pasillo;
import Escapeguard.Game.GameController;
import Escapeguard.Game.Sprites.Corazon;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.List;

import static Escapeguard.Game.GameController.tempLadrillo;

public class CrearComponentes {

    private Pasillo pasillo;

    public CrearComponentes(Pasillo pasillo) {
        this.pasillo = pasillo;
    }

    // Crea una lista de ladrillos
    public List<Ladrillo> crearListaLadrillos(int numero, GraphicsContext graphicsContext) {
        List<Ladrillo> ladrillos = new ArrayList<>();
        int x = 0;
        int y = 0;
        for (int i = 0; i < numero; i++) {
            // Dibujo arriba
            if (i < numero / 2 )  {
                y = pasillo.getPositionY() - tempLadrillo.getHeight();
            }
            // Dibujo abajo
            else {
                y = pasillo.getPositionY() + pasillo.getHeight();
            }
            ladrillos.add(new Ladrillo(new Image(Ladrillo.ruta), 20,20, x,y));
            ladrillos.get(i).setGraphicsContext(graphicsContext);
            ladrillos.get(i).render();
            x += tempLadrillo.getWidth();
            // Cuando es el ultimo ladrillo de la primera fila
            if (i + 1 == numero / 2) {
                x = 0;
            }
        }
        return ladrillos;
    }

    // Crea una lista de habitaciones
    public List<Habitacion> crearListaHabitaciones(int numero, GraphicsContext graphicsContext) {
        List<Habitacion> habitaciones = new ArrayList<>();
        int x = 0;
        int y = 0;
        int diferencia = GameController.widthScreen / (numero / 2);
        Habitacion temp = new Habitacion(new Image(Habitacion.ruta), x,y);
        for (int i = 0; i < numero; i++) {
            // Dibujo arriba
            if (i < (numero / 2)) {
                y = pasillo.getPositionY() - temp.getHeight();
            }
            // Dibujo abajo
            else {
                y = pasillo.getPositionY() + pasillo.getHeight();
            }
            // Creo las habitaciones
            habitaciones.add(new Habitacion(new Image(Habitacion.ruta), x,y));
            habitaciones.get(i).setGraphicsContext(graphicsContext);
            habitaciones.get(i).render();
            // Obtengo una nueva posicion para la siguiente habitaciones
            x += (int) (Math.random() * diferencia + temp.getWidth());
            // Si es la ultima habitacion de arriba
            if ((numero / 2) - 1 == i) {
                x = 0;
            }
        }
        return habitaciones;
    }

    // Devuelve una lista de corazones
    public Corazon[] crearCorazones(int numero, GraphicsContext graphicsContext) {
        Corazon[] corazones = new Corazon[numero];
        Corazon temp = new Corazon(new Image(Corazon.ruta), 0,0);
        int x;
        for (int i = 0; i < numero; i++) {
            // Obtengo la posicion x
            x = GameController.widthScreen - (temp.getWidth() * (numero - i));
            corazones[i] = new Corazon(new Image(Corazon.ruta),  x, 0);
            corazones[i].setGraphicsContext(graphicsContext);
        }
        return corazones;
    }
}
