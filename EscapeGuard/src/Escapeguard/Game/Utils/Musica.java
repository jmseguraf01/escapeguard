package Escapeguard.Game.Utils;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class Musica {

    private final String audioJuego = "src/EscapeGuard/Game/Audio/game.mp3";
    private final String audioMuerto = "src/EscapeGuard/Game/Audio/muerto.mp3";
    private final String audioGanado = "src/EscapeGuard/Game/Audio/ganado.mp3";

    public MediaPlayer juego() {
        Media sound = new Media(new File(audioJuego).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        return mediaPlayer;
    }

    public MediaPlayer muerto() {
        Media sound = new Media(new File(audioMuerto).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        return mediaPlayer;
    }

    public MediaPlayer ganado() {
        Media sound = new Media(new File(audioGanado).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        return mediaPlayer;
    }
}
