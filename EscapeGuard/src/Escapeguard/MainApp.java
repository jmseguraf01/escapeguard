package Escapeguard;

import Escapeguard.Game.MenuController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{


        FXMLLoader loader = new FXMLLoader(getClass().getResource("Game/menustart.fxml"));
        Parent root = loader.load();

        primaryStage.setTitle("Escape Guard");
        primaryStage.setResizable(false);

        Scene scene = new Scene(root);

        MenuController menuController = loader.getController();
        menuController.setScene(scene);
        menuController.setStage(primaryStage);
        scene.getStylesheets().add("styles.css");

        primaryStage.setScene(scene);
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
